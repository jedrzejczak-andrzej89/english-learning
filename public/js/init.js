'use strict';

var app = angular.module('app', ['ngResource', 'ngRoute']);

app.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
        otherwise({
            templateUrl: '/js/templates/words/index.html',
            controller: 'WordIndexController',
            resolve: {
                words: ['Word', function (Word) {
                    return Word.all();
                }]
            }
        });
    }]);