'use strict';

app.factory('Word', function ($http) {
    var path = 'api/words';

    return {
        all: function () {
            return $http.get(path);
        }
    };
});