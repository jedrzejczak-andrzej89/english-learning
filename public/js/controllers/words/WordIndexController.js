'use strict';

app.controller('WordIndexController', ['$scope', 'words', function($scope, words) {

    var words = words.data;

    $scope.wordCounter = 0;
    $scope.flipped = '';

    getWord();

    $scope.revert = function() {
        if (isFront()) {
            showBack();
        } else if (isBack()) {
            showFront();
        }
    };

    $scope.know_this_word = function() {
        next();
        showFront();
    };

    $scope.dont_know_this_word = function() {
        next();
        showFront();
    };

    $scope.previous_word = function() {
        previous();
    };

    $scope.next_word = function() {
        next();
    };

    function next() {
        if ($scope.wordCounter == words.length - 1) {
            $scope.wordCounter = 0;
        } else {
            $scope.wordCounter++;
        }
        getWord();
    }

    function previous() {
        if ($scope.wordCounter == 0) {
            $scope.wordCounter = words.length - 1
        } else {
            $scope.wordCounter--;
        }
        getWord();
    }


    function isFront() {
        return ($scope.flipped.length == 0);
    }

    function isBack() {
        return ($scope.flipped == 'flipped');
    }

    function showFront() {
        $scope.flipped = '';
    }

    function showBack() {
        $scope.flipped = 'flipped';
    }

    function getWord() {
        $scope.word = words[$scope.wordCounter];
    }

}]);
