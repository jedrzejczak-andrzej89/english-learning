<!DOCTYPE html>
<html>
    <head>
        <title>English learning</title>
        <script src="/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/flip.css">

        <script src="/bower_components/angular/angular.min.js"></script>
        <script src="/bower_components/angular-resource/angular-resource.min.js"></script>
        <script src="/bower_components/angular-route/angular-route.min.js"></script>

        <script src="/js/init.js"></script>
        <script src="/js/services/Word.js"></script>
        <script src="/js/controllers/words/WordIndexController.js"></script>
    </head>
    <body>
    <div class="container" ng-app="app">
        <div ng-view></div>
    </div>
    </body>
</html>
