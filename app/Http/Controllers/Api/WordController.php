<?php

namespace App\Http\Controllers\Api;

use App\Word;

class WordController extends ApiController
{
    public function index()
    {
        return Word::orderByRaw("RAND()")->get();
    }
}
